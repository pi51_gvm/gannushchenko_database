SELECT dbo_student.Fname, dbo_student.Name, predmet.Nazva, SUM(Reiting.Reiting) FROM dbo_student
INNER JOIN Reiting ON Reiting.Kod_student=dbo_student.Kod_stud
INNER JOIN  Rozklad_pids ON Rozklad_pids.K_zapis=Reiting.K_zapis
INNER JOIN Predmet_plan ON Predmet_plan.K_predm_pl=Rozklad_pids.K_predm_pl
INNER JOIN predmet ON predmet.K_predmet=Predmet_plan.K_predmet
GROUP BY dbo_student.Fname, dbo_student.Name, predmet.Nazva;


SELECT dbo_student.Kod_group, COUNT(dbo_student.Kod_stud) AS Total FROM dbo_student
GROUP BY dbo_student.Kod_group;

SELECT dbo_groups.Kod_group, COUNT(predmet.K_predmet) FROM dbo_groups
INNER JOIN Rozklad_pids ON Rozklad_pids.Kod_group=dbo_groups.Kod_group
INNER JOIN Predmet_plan ON Rozklad_pids.K_predm_pl= Predmet_plan.K_predm_pl
INNER JOIN predmet ON Predmet_plan.K_predmet = predmet.K_predmet
GROUP BY dbo_groups.Kod_group;


SELECT dbo_groups.Kod_group, AVG(Reiting.Reiting) FROM.dbo_groups
INNER JOIN dbo_student ON dbo_student.Kod_group=dbo_groups.Kod_group
INNER JOIN Reiting ON Reiting.Kod_student=dbo_student.Kod_stud
GROUP BY dbo_groups.Kod_group

SELECT predmet.Nazva, AVG(Reiting.Reiting) FROM predmet
INNER JOIN Predmet_plan ON predmet.K_predmet=Predmet_plan.K_predmet
INNER JOIN Rozklad_pids ON Rozklad_pids.K_predm_pl=Predmet_plan.K_predm_pl
INNER JOIN Reiting ON Reiting.K_zapis= Rozklad_pids.K_zapis
GROUP BY  predmet.Nazva;

SELECT dbo_student.Sname,dbo_student.Name, predmet.Nazva, MIN(Reiting.Reiting) FROM predmet
INNER JOIN Predmet_plan ON predmet.K_predmet=Predmet_plan.K_predmet
INNER JOIN Rozklad_pids ON Rozklad_pids.K_predm_pl=Predmet_plan.K_predm_pl
INNER JOIN Reiting ON Reiting.K_zapis= Rozklad_pids.K_zapis
INNER JOIN dbo_student ON dbo_student.Kod_stud=Reiting.Kod_student
GROUP BY predmet.Nazva,dbo_student.Sname,dbo_student.Name

SELECT dbo_student.Sname,dbo_student.Name, predmet.Nazva, MAX(Reiting.Reiting) FROM predmet
INNER JOIN Predmet_plan ON predmet.K_predmet=Predmet_plan.K_predmet
INNER JOIN Rozklad_pids ON Rozklad_pids.K_predm_pl=Predmet_plan.K_predm_pl
INNER JOIN Reiting ON Reiting.K_zapis= Rozklad_pids.K_zapis
INNER JOIN dbo_student ON dbo_student.Kod_stud=Reiting.Kod_student
GROUP BY predmet.Nazva,dbo_student.Sname,dbo_student.Name

SELECT predmet.Nazva, SUM(Predmet_plan.Chas_sem) AS Sem, SUM(Predmet_plan.Chas_Lek) AS Lek, SUM(Predmet_plan.Chas_Labor) AS lab FROM predmet
LEFT JOIN Predmet_plan ON Predmet_plan.K_predmet=predmet.K_predmet
GROUP BY predmet.Nazva

SELECT Spetsialnost.Nazva, COUNT(dbo_groups.Kod_group) FROM Spetsialnost
INNER JOIN Navch_plan ON Navch_plan.K_spets=Spetsialnost.K_spets
INNER JOIN Predmet_plan ON Predmet_plan.K_navch_plan=Navch_plan.K_navch_plan
INNER JOIN Rozklad_pids ON Rozklad_pids.K_predm_pl=Predmet_plan.K_predm_pl
INNER JOIN dbo_groups ON Rozklad_pids.Kod_group=dbo_groups.Kod_group
GROUP BY Spetsialnost.Nazva