--������ �������� � ��� ������ ��� �� ����������� �����
CREATE VIEW qresult AS
SELECT zakaz_tovar.K_zakaz, klient.Nazva, zakaz_tovar.k_tovar,
zakaz_tovar.Kilkist, zakaz_tovar.Znigka, tovar.Price*zakaz_tovar.Kilkist*(1-
zakaz_tovar.Znigka/100) AS Zag_vartist
FROM (klient INNER JOIN zakaz ON klient.K_klient = zakaz.K_klient) INNER
JOIN (tovar INNER JOIN zakaz_tovar ON tovar.K_tovar =
zakaz_tovar.k_tovar) ON zakaz.K_zakaz = zakaz_tovar.K_zakaz;


CREATE VIEW qresult2 AS
SELECT qresult.K_zakaz, klient.Nazva, zakaz.date_naznach,
Sum(qresult.Zag_vartist) AS Itog
FROM (klient INNER JOIN zakaz ON klient.K_klient=zakaz.K_klient) INNER JOIN
qresult ON zakaz.K_zakaz = qresult.K_zakaz
GROUP BY qresult.K_zakaz, klient.Nazva, zakaz.date_naznach;


SELECT sotrudnik.Fname, sotrudnik.Name, sotrudnik.Posada,
Avg(qresult2.Itog) AS [Avg-Itog]
FROM (sotrudnik INNER JOIN zakaz ON sotrudnik.K_sotrud=zakaz.k_sotrud)
INNER JOIN qresult2 ON zakaz.K_zakaz=qresult2.K_zakaz
GROUP BY sotrudnik.Fname, sotrudnik.Name, sotrudnik.Posada;


SELECT sotrudnik.Fname, sotrudnik.Name, sotrudnik.Posada,
Avg(qresult2.Itog) AS [Avg-Itog]
FROM (sotrudnik INNER JOIN zakaz ON sotrudnik.K_sotrud=zakaz.k_sotrud)
INNER JOIN qresult2 ON zakaz.K_zakaz=qresult2.K_zakaz
GROUP BY sotrudnik.Fname, sotrudnik.Name, sotrudnik.Posada;


SELECT klient.Nazva, Sum(qresult.Zag_vartist) AS [Sum-Zag_vartist]
FROM (klient INNER JOIN zakaz ON klient.K_klient = zakaz.K_klient) INNER
JOIN qresult ON zakaz.K_zakaz = qresult.K_zakaz
GROUP BY klient.Nazva;


SELECT tovar.Nazva, Max((tovar.Price*zakaz_tovar.Kilkist*(1-
zakaz_tovar.Znigka/100))) AS Zag_vartist
FROM tovar INNER JOIN zakaz_tovar ON tovar.K_tovar=zakaz_tovar.k_tovar
GROUP BY tovar.Nazva;


SELECT sotrudnik.Fname, sotrudnik.Name, sotrudnik.Posada,
Count(zakaz.K_zakaz) AS [Count-K_zakaz]
FROM zakaz INNER JOIN sotrudnik ON zakaz.k_sotrud = sotrudnik.K_sotrud
GROUP BY sotrudnik.Fname, sotrudnik.Name, sotrudnik.Posada;


--�������� ��������� ������
SELECT postachalnik.Nazva,  Sum(tovar.NaSklade) FROM postachalnik
LEFT JOIN tovar ON tovar.K_postav=postachalnik.K_postach
GROUP BY postachalnik.Nazva;


SELECT klient.Nazva, COUNT(zakaz.k_sotrud) AS Sotrud FROM klient
LEFT JOIN  zakaz ON zakaz.K_klient=klient.K_klient
GROUP BY klient.Nazva;

SELECT klient.Nazva, COUNT(tovar.K_postav) AS Postav FROM klient
LEFT JOIN zakaz ON zakaz.K_klient=klient.K_klient
LEFT JOIN zakaz_tovar ON zakaz.K_zakaz=zakaz_tovar.K_zakaz
LEFT JOIN tovar ON zakaz_tovar.k_tovar=tovar.K_tovar
GROUP BY klient.Nazva ; 

SELECT tovar.Nazva,SUM(zakaz_tovar.Kilkist) AS TOTAL FROM tovar
LEFT JOIN zakaz_tovar ON zakaz_tovar.k_tovar=tovar.K_tovar
INNER JOIN zakaz ON zakaz_tovar.K_zakaz=zakaz.K_zakaz AND MONTH(zakaz.date_naznach)=3
GROUP BY tovar.Nazva;

SELECT SUM(zakaz_tovar.Kilkist*tovar.Price*(1-zakaz_tovar.Znigka/100)) FROM zakaz
INNER JOIN zakaz_tovar ON zakaz_tovar.K_zakaz=zakaz.K_zakaz AND MONTH(zakaz.date_naznach)=3
INNER JOIN tovar ON zakaz_tovar.k_tovar=tovar.K_tovar
GROUP BY MONTH(zakaz.date_naznach);


SELECT postachalnik.Nazva, SUM(zakaz_tovar.Kilkist*tovar.Price*(1-zakaz_tovar.Znigka/100)) FROM zakaz_tovar
INNER JOIN tovar ON tovar.K_tovar=zakaz_tovar.k_tovar
RIGHT JOIN postachalnik ON postachalnik.K_postach=tovar.K_postav
GROUP BY postachalnik.Nazva

SELECT postachalnik.Nazva, COUNT(zakaz.K_zakaz) FROM postachalnik
LEFT JOIN tovar ON postachalnik.K_postach=tovar.K_postav AND tovar.Nazva Like 'Ca%'
LEFT JOIN zakaz_tovar ON tovar.K_tovar=zakaz_tovar.k_tovar
LEFT JOIN zakaz ON zakaz.K_zakaz=zakaz_tovar.K_zakaz
GROUP BY postachalnik.Nazva


SELECT tovar.Nazva, AVG(zakaz_tovar.Kilkist*tovar.Price*(1-zakaz_tovar.Znigka/100)) FROM zakaz_tovar
INNER JOIN tovar ON tovar.K_tovar=zakaz_tovar.k_tovar
GROUP BY tovar.Nazva


SELECT SUM(zakaz_tovar.Kilkist*tovar.Price*(1-zakaz_tovar.Znigka/100)) AS TOTAL FROM klient
INNER JOIN zakaz ON klient.City LIKE 'Zhytomyr' AND klient.K_klient=zakaz.K_zakaz
INNER JOIN zakaz_tovar ON zakaz_tovar.K_zakaz=zakaz.K_zakaz
INNER JOIN tovar ON zakaz_tovar.k_tovar=tovar.K_tovar
GROUP BY klient.City

SELECT postachalnik.Nazva, AVG(tovar.Price) AS AVG_price FROM postachalnik
INNER JOIN tovar ON tovar.K_postav=postachalnik.K_postach
GROUP BY postachalnik.Nazva

